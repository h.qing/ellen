package log

import (
	"fmt"
)

const (
	begin = "\033["
	end   = "\033[0m"
)

// Foreground bright
const (
	fgBlack   = "30;1"
	fgRed     = "31;1"
	fgGreen   = "32;1"
	fgYellow  = "33;1"
	fgBlue    = "34;1"
	fgMagenta = "35;1"
	fgCyan    = "36;1"
	fgWhite   = "37;1"
)

// Foreground dark
const (
	fgDarkBlack   = "30"
	fgDarkRed     = "31"
	fgDarkGreen   = "32"
	fgDarkYellow  = "33"
	fgDarkBlue    = "34"
	fgDarkMagenta = "35"
	fgDarkCyan    = "36"
	fgDarkWhite   = "37"
)

// Background bright
const (
	bgBlack   = "40;1"
	bgRed     = "41;1"
	bgGreen   = "42;1"
	bgYellow  = "43;1"
	bgBlue    = "44;1"
	bgMagenta = "45;1"
	bgCyan    = "46;1"
	bgWhite   = "47;1"
)

func printColor(format string, color string, v ...interface{}) {
	fmt.Printf(begin+color+"m"+format+end, v...)
}

// system debug printf
func Debugf(format string, v ...interface{}) {
	printColor(format, fgDarkWhite, v...)
}

// system info printf
func Infof(format string, v ...interface{}) {
	printColor(format, fgWhite, v...)
}

// system warn printf
func Warnf(format string, v ...interface{}) {
	printColor(format, fgYellow, v...)
}

// system error printf
func Errorf(format string, v ...interface{}) {
	printColor(format, fgRed, v...)
}

// system self printf
func Sysf(format string, v ...interface{}) {
	printColor(format, fgBlue, v...)
}

// system output log printf
func Outf(format string, v ...interface{}) {
	printColor(format, fgGreen, v...)
}
