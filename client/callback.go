package client

import (
	"encoding/json"

	"github.com/pkg/errors"
	"kuaicuocuo.com/ellen/log"

	//apint "kuaicuocuo.com/ellen/resources"
	apint "kuaicuocuo.com/indy/apint"
)

//var step = make(chan bool, 1)

type (
	messageHandler struct{}
)

///////////////////////////////////////// CALL BACK //////////////////////////////////////////////
func (m *messageHandler) OnConnection(conn *apint.Connection) {
	log.Sysf("New connection: %s\n", conn.ConnectionID)
	go client.addConn(conn.ConnectionID)
}

func (m *messageHandler) OnBasicMessage(message, from, to string) error {
	return handleOnBasicMessage(message)
}

func handleOnBasicMessage(message string) error {
	switch parseMsgType(message) {
	//register did by alice
	case "DidRegisterResponse":
		return activeDidByMessageCallback(message)
	default:
		log.Sysf("Receive message:\n%s\n", message)
	}
	return nil

}

func parseMsgType(message string) string {
	type _type struct {
		Type string
	}
	t := &_type{}
	err := json.Unmarshal([]byte(message), t)
	if err != nil {
		return ""
	}
	return t.Type
}

func activeDidByMessageCallback(message string) error {
	type _DV struct {
		Did    string
		Verkey string
	}

	type _Data struct {
		Status  int32
		Message string
		Data    _DV
	}

	type _dv struct {
		Type string
		Data _Data
	}

	dv := &_dv{
		Data: _Data{Data: _DV{}},
	}

	err := json.Unmarshal([]byte(message), dv)
	if err != nil {
		return err
	}

	if dv.Data.Status != 100 {
		return errors.Errorf("status=%d,message=%s", dv.Data.Status, dv.Data.Message)
	}

	_, err = activeDidVerkey(dv.Data.Data.Did, dv.Data.Data.Verkey)
	if err != nil {
		return err
	}
	return nil
}
