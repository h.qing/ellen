package client

import (
	"fmt"

	"github.com/pkg/errors"
	"kuaicuocuo.com/ellen/log"

	//apint "kuaicuocuo.com/ellen/resources"
	apint "kuaicuocuo.com/indy/apint"
)

const (
	AlicePeerInv = `{"serviceEndpoint":"zmq://52.82.70.44:30893","recipientKeys":["4717kjEprVDrM7G2A6UjRqPc8VYKvyfeGEj8bxLsoDjC"],"@id":"a9fa8866-e3de-43a7-a0c8-84f8b2468bb9","label":"iEphRwGkwe","@type":"https://didcomm.org/didexchange/1.0/invitation"}`
	AliceInv     = `{"@id":"216c7d6d-aff7-4f7b-957a-ee08db3eb00d","label":"Alice-Public-Invitation","did":"did:sov:A7a1JeH4hQStvN7xcNbFwB","@type":"https://didcomm.org/didexchange/1.0/invitation"}`
)

var client *_client
var tmp *_tmp

type _tmp struct {
	Name string
}
type _connection struct {
	AliasName string
	Tip       string
	*apint.Connection
}
type _client struct {
	Name       string
	cmdHistroy []string
	PK         apint.KccSsiPlugin
	DI         *apint.DidInfo
	Conns      map[string]*_connection
}

func init() {
	client = &_client{
		Name:       "",
		cmdHistroy: make([]string, 0),
		PK:         nil,
		DI:         &apint.DidInfo{},
		Conns:      make(map[string]*_connection, 0),
	}

	tmp = &_tmp{
		Name: "Unkown",
	}

}

func (c *_client) getConn(s string) (*_connection, error) {
	// by connection id
	if conn, ok := c.Conns[s]; ok == true {
		return conn, nil
	}
	localConn := make(map[string]*_connection)
	// by alias name
	tmpConn := &_connection{}
	for _, conn := range c.Conns {
		if conn.AliasName == s {
			localConn[conn.ConnectionID] = conn
			tmpConn = conn
		}
	}

	if len(localConn) == 1 {
		return tmpConn, nil
	} else if len(localConn) > 1 {
		i := 0
		log.Warnf("At least two same name in your wallet.")
		for _, conn := range localConn {
			i++
			log.Outf("%3d. name:%s conn id:%s\n", i, conn.AliasName, conn.ConnectionID)
		}
		iconn := Input("Please input a connection id:")
		c.getConn(iconn)
	}

	//TODO by did
	return nil, errors.New("failed to find connection.")
}

func (c *_client) addConn(connId string) error {
	// by connection id
	if _, err := c.getConn(connId); err == nil {
		log.Warnf("Already in your wallet.\n")
		return nil
	} else {
		_con, err := client.PK.GetConnection(connId)
		if err != nil {
			return errors.New("failed to find connection.")
		}

		name := Input(fmt.Sprintf("Input a conn id(%s) alias name:", connId))
		//name := "Unkown"
		con := &_connection{
			name,
			"",
			_con,
		}

		c.Conns[connId] = con
		return nil
	}

	return errors.New("failed to find connection.")
}
