package client

import (
	"bufio"
	"os"
	"strconv"
	"strings"

	"kuaicuocuo.com/ellen/log"
)

// waiting inputs, return strings
func Inputs(tip ...string) []string {
	if len(tip) != 0 {
		log.Sysf(tip[0])
	}
	is := make([]string, 0)
	input, _ := bufio.NewReader(os.Stdin).ReadString('\n')
	input = strings.Replace(input, "\n", "", -1)
	_is := strings.Split(input, " ")
	for _, i := range _is {
		i = strings.Replace(i, "\n", "", -1)
		if i != "" && i != " " {
			is = append(is, strings.TrimSpace(i))
		}
	}

	return is
}

// waiting input, return string
func Input(tip ...string) string {
	if len(tip) != 0 {
		log.Sysf(tip[0])
	}
	input, _ := bufio.NewReader(os.Stdin).ReadString('\n')
	input = strings.Replace(input, "\n", "", -1)
	input = strings.TrimSpace(input)
	return input
}

// waiting input, return int
func InputInt(tip ...string) int {
	inp := Input(tip...)
	in, err := strconv.Atoi(inp)
	if err != nil {
		return -1
	}
	return in
}

//func (s fcsA) Len() int           { return len(s) }
//func (s fcsA) Swap(i, j int)      { s[i], s[j] = s[j], s[i] }
//func (s fcsA) Less(i, j int) bool { return s[i].Index < s[j].Index }
