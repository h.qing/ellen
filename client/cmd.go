package client

import (
	"context"
	"fmt"
	"math/rand"
	"os"
	"time"

	"github.com/spf13/cobra"
	"kuaicuocuo.com/ellen/log"
)

var clientCmd = &cobra.Command{
	Long: "Your wallet.",
}

func init() {
	clientCmd.Execute()
	versionCmd := &cobra.Command{
		Use:   "version",
		Short: `Show sdk version.`,
		Long:  `Show sdk version.`,
		Run: func(cmd *cobra.Command, args []string) {
			getVersion()
		},
	}

	initWalletCmd := &cobra.Command{
		Use:   "init [OPTIONS]",
		Short: `Init a new wallet.Input your name, wallet plugin path and gensis file.`,
		Long:  `Init a new wallet.Input your name, wallet plugin path and gensis file.`,
		Args:  cobra.ArbitraryArgs,
		Run: func(cmd *cobra.Command, args []string) {
			pf := ""
			gf := ""
			iName := RandomString(5)
			if len(args) == 1 {
				if args[0] != "" {
					iName = args[0]
				}
			}
			initWallet(pf, gf, iName)
		},
	}

	openWalletCmd := &cobra.Command{
		Use:   "open [OPTIONS]",
		Short: `Open your wallet.`,
		Long:  `Open your wallet.`,
		Args:  cobra.MinimumNArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			pf := ""
			gf := ""
			iName := ""
			if len(args) == 1 {
				if args[0] != "" {
					iName = args[0]
				}
			}
			initWallet(pf, gf, iName)
		},
	}

	quickStartCmd := &cobra.Command{
		Use:     "quickstart",
		Aliases: []string{"qs"},
		Short:   `Quick start - Register Did by Alice and active it.`,
		Long:    `Quick start - Register Did by Alice and active it.`,
		Run: func(cmd *cobra.Command, args []string) {
			err := quickStart()
			if err != nil {
				log.Warnf("quick start err:%s.\n", err)
			}
		},
	}

	whoAmICmd := &cobra.Command{
		Use:     "who",
		Aliases: []string{"me"},
		Short:   `Who am I?`,
		Long:    `Who am I?`,
		Run: func(cmd *cobra.Command, args []string) {
			whoAmI("")
		},
	}

	exitCmd := &cobra.Command{
		Use:     "exit",
		Aliases: []string{"quit", "exit", "bye"},
		Short:   `Quit wallet.`,
		Long:    `Quit wallet.`,
		Run: func(cmd *cobra.Command, args []string) {
			log.Outf("Bye!\n")
			os.Exit(0)
		},
	}

	moreWhoAmICmd := &cobra.Command{
		Use:   "more",
		Short: `Who am I more info?`,
		Long:  `Who am I more info?`,
		Run: func(cmd *cobra.Command, args []string) {
			whoAmI("more")
		},
	}

	createCmd := &cobra.Command{
		Use:   "create [OPTIONS]",
		Short: `Create invitation or seed.`,
		Long:  `Create invitation or seed.`,
		Run: func(cmd *cobra.Command, args []string) {
			log.Warnf("create parameter cannot be null.\n")
		},
	}

	getCmd := &cobra.Command{
		Use:   "get [OPTIONS]",
		Short: `Get resources.`,
		Long:  `Get resources.`,
		Run: func(cmd *cobra.Command, args []string) {
			log.Warnf("get parameter cannot be null.\n")
		},
	}

	getLinksCmd := &cobra.Command{
		Use:   "link",
		Short: `Get all links.`,
		Long:  `Get all links.`,
		Run: func(cmd *cobra.Command, args []string) {
			listAllMessager()
		},
	}

	setCmd := &cobra.Command{
		Use:   "set [OPTIONS]",
		Short: `Set resources.`,
		Long:  `set resources.`,
		Run: func(cmd *cobra.Command, args []string) {
			log.Warnf("set parameter cannot be null.\n")
		},
	}

	setConnAliasNameCmd := &cobra.Command{
		Use:     "connectionaliasname [OPTIONS]",
		Aliases: []string{"connalias"},
		Short:   `Set connection alias name.`,
		Long:    `Set connection alias name.`,
		Args:    cobra.RangeArgs(2, 2),
		Run: func(cmd *cobra.Command, args []string) {
			if args[0] != "" && args[1] != "" {
				base := args[0]
				seed := args[1]
				setConnectionAliasName(base, seed)
			} else {
				log.Warnf("set parameter cannot be null.\n")
			}
		},
	}

	invitationCmd := &cobra.Command{
		Use:     "invitation [OPTIONS]",
		Aliases: []string{"inv"},
		Short:   `Create a invitation.`,
		Long:    `Create a invitation.`,
		Run: func(cmd *cobra.Command, args []string) {
		},
	}

	createDidCmd := &cobra.Command{
		Use:       "did [OPTIONS]",
		Short:     `Create a did and verkey.`,
		Long:      `Create a did and verkey.`,
		Args:      cobra.OnlyValidArgs,
		ValidArgs: []string{"base", "seed"},
		Run: func(cmd *cobra.Command, args []string) {
			base := false
			seed := ""
			createPublicDid(base, seed)
		},
	}

	createDidByBaseCmd := &cobra.Command{
		Use:   "base [OPTIONS]",
		Short: `Create a did and verkey by (create and register).`,
		Long:  `Create a did and verkey by (create and register).`,
		Run: func(cmd *cobra.Command, args []string) {
			base := true
			seed := ""
			createPublicDid(base, seed)
		},
	}

	createDidBySeedCmd := &cobra.Command{
		Use:   "seed [OPTIONS]",
		Short: `Create a did and verkey by seed.`,
		Long:  `Create a did and verkey by seed.`,
		Args:  cobra.RangeArgs(1, 1),
		Run: func(cmd *cobra.Command, args []string) {
			base := false
			seed := ""
			if args[0] != "" {
				seed = args[0]
				createPublicDid(base, seed)
			} else {
				log.Warnf("seed parameter cannot be null.\n")
			}
		},
	}

	peerInvitationCmd := &cobra.Command{
		Use:     "peer",
		Aliases: []string{"label"},
		Short:   `Create a invitation by label, the label is default name.`,
		Long:    `Create a invitation by label, the label is default name.`,
		Run: func(cmd *cobra.Command, args []string) {
			createInvitation(client.Name, "")
		},
	}

	publicInvitationCmd := &cobra.Command{
		Use:     "public",
		Aliases: []string{"pub", "did"},
		Short:   `Create a invitation by label and did, the label is default name.`,
		Long:    `Create a invitation by label and did, the label is default name.`,
		Args:    cobra.RangeArgs(1, 1),
		Run: func(cmd *cobra.Command, args []string) {
			if args[0] != "" {
				did := args[0]
				createInvitation(client.Name, did)
			} else {
				log.Warnf("public parameter cannot be null.\n")
			}
		},
	}

	sendMessageCmd := &cobra.Command{
		Use:     "message",
		Aliases: []string{"msg"},
		Short:   `Send a message by connection id.`,
		Long:    `Send a message by connection id.`,
		Args:    cobra.RangeArgs(1, 1),
		PreRun: func(cmd *cobra.Command, args []string) {
			listAllMessager()
		},
		Run: func(cmd *cobra.Command, args []string) {
			if args[0] != "" {
				name := args[0]
				if conn, err := client.getConn(name); err == nil {
					holdSendMessage(conn)
				}
			} else {
				log.Warnf("message parameter cannot be null.\n")
			}
		},
	}

	registerCmd := &cobra.Command{
		Use:     "register [OPTIONS]",
		Aliases: []string{"reg"},
		Short:   `Register resources..`,
		Long:    `Register resources.`,
		Run: func(cmd *cobra.Command, args []string) {
			log.Warnf("register parameter cannot be null.\n")
		},
	}

	registerRouterCmd := &cobra.Command{
		Use:     "router",
		Aliases: []string{"r"},
		Short:   `Register connection id with router.`,
		Long:    `Register connection id with router.`,
		Args:    cobra.RangeArgs(1, 1),
		Run: func(cmd *cobra.Command, args []string) {
			if args[0] != "" {
				name := args[0]
				registerWithRouter(name)
			} else {
				log.Warnf("public parameter cannot be null.\n")
			}
		},
	}

	registerInvitationCmd := &cobra.Command{
		Use:     "invitation",
		Aliases: []string{"inv"},
		Args:    cobra.RangeArgs(1, 1),
		Short:   `Register handle a invitation.`,
		Long:    `Register handle a invitation.`,
		Run: func(cmd *cobra.Command, args []string) {
			if args[0] != "" {
				inv := args[0]
				_, err := handleInvitation(inv)
				if err != nil {
					log.Errorf("handle invitation %s.\n", err.Error())
				}
			} else {
				log.Warnf("invitation parameter cannot be null.\n")
			}
		},
	}

	clientCmd.AddCommand(createCmd)
	createCmd.AddCommand(invitationCmd)
	createCmd.AddCommand(createDidCmd)
	createDidCmd.AddCommand(createDidByBaseCmd)
	createDidCmd.AddCommand(createDidBySeedCmd)
	invitationCmd.AddCommand(peerInvitationCmd)
	invitationCmd.AddCommand(publicInvitationCmd)

	clientCmd.AddCommand(exitCmd)
	clientCmd.AddCommand(getCmd)
	clientCmd.AddCommand(setCmd)
	clientCmd.AddCommand(registerCmd)
	clientCmd.AddCommand(sendMessageCmd)
	getCmd.AddCommand(getLinksCmd)
	setCmd.AddCommand(setConnAliasNameCmd)

	clientCmd.AddCommand(versionCmd, initWalletCmd, quickStartCmd, whoAmICmd, openWalletCmd)

	whoAmICmd.AddCommand(moreWhoAmICmd)
	registerCmd.AddCommand(registerRouterCmd)
	registerCmd.AddCommand(registerInvitationCmd)
}

func Start(ctx context.Context) {
	//for {
	//	select {
	//	case <-step:
	//		cmds := Inputs(host())
	//		if len(cmds) == 0 {
	//			continue
	//		}

	//		clientCmd.SetArgs(cmds)
	//		clientCmd.Execute()
	//	case <-time.After(2 * time.Second):
	//		step <- true
	//	}
	//}
	for {
		cmds := Inputs(host())
		if len(cmds) == 0 {
			continue
		}

		clientCmd.SetArgs(cmds)
		clientCmd.Execute()
	}
}

//TODO
func appContain(in string) string {
	return ""
}

func host() string {
	h := ">"
	if client.PK == nil {
		return h
	} else {
		if client.Name != "" {
			h = fmt.Sprintf("%s>", client.Name)
		}
		if client.DI != nil {
			if client.DI.Did != "" {
				sid := client.DI.Did
				if len(sid) >= 8 {
					sid = sid[len(sid)-8:]
				}
				h = fmt.Sprintf("~%s:%s", sid, h)
			}
		}
		return h
	}
	return h
}

var defaultLetters = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

func RandomString(n int, allowedChars ...[]rune) string {
	var letters []rune

	if len(allowedChars) == 0 {
		letters = defaultLetters
	} else {
		letters = allowedChars[0]
	}

	b := make([]rune, n)

	rand.Seed(time.Now().UnixNano())
	for i := range b {
		b[i] = letters[rand.Intn(len(letters))]
	}

	return string(b)
}
