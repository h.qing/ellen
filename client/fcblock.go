package client

import (
	"fmt"
	"os"
	"plugin"
	"time"

	"github.com/google/uuid"
	"github.com/pkg/errors"
	"kuaicuocuo.com/ellen/log"
	//apint "kuaicuocuo.com/ellen/resources"
	apint "kuaicuocuo.com/indy/apint"
)

const (
	CLOSEMESSAGE = "@close message"
)

///////////////////////////////////////// FUNCTIONS //////////////////////////////////////////////
// Init wallet.
func initWallet(pf, gf, name string) error {
	if client.PK != nil {
		log.Warnf("Already init.\n")
		return nil
	}

	if pf == "" || gf == "" {
		//pf = fmt.Sprintf("/home/seaii/kcc/workSpace/ellen/resources/ssiplugin.so.%s", apint.API_VERSION)
		//gf = "/home/seaii/kcc/workSpace/ellen/resources/pool_genesis_ip"
		pf = fmt.Sprintf("/home/seaii/kcc/workSpace/indy/apint/ssiplugin.so.%s", apint.API_VERSION)
		gf = "/home/seaii/kcc/workSpace/indy/apint/pool_genesis_ip"

		log.Infof("Default genesis and sdk.\n")
	}

	if name == "" {
		name = tmp.Name
	}

	log.Infof("start login name=%s\n", name)
	return initClient(pf, gf, name)
}

func initClient(pf, gf, name string) error {
	plugin, err := plugin.Open(pf)
	if err != nil {
		log.Errorf("%v\n", errors.Wrap(err, "failed to plugin open"))
		os.Exit(0)
	}

	symKccSsiPlugin, err := plugin.Lookup("KccSsiPlugin")
	if err != nil {
		log.Errorf("%v\n", errors.Wrap(err, "failed to plugin look up"))
		os.Exit(0)
	}

	ksp, ok := symKccSsiPlugin.(apint.KccSsiPlugin)
	if !ok {
		log.Errorf("%v\n", errors.New("failed to rnexpected type from module symbol"))
		os.Exit(0)
	}

	ksp.NewKccSsiInstance()

	ksp.SetPoolName(fmt.Sprintf("%s-pool", name))
	ksp.SetGenesisFilePath(gf)
	ksp.SetWalletName(fmt.Sprintf("%s-wallet", name))
	ksp.SetWalletPath(fmt.Sprintf("/tmp/%s-wallet/", name))

	if err := ksp.InitZmqClient(); err != nil {
		log.Errorf("%v\n", errors.Wrap(err, "failed to initialize communication channel"))
		os.Exit(0)
	}

	if err := ksp.Init(); err != nil {
		log.Errorf("%v\n", errors.Wrap(err, "failed to initialize kcc ssi"))
		os.Exit(0)
	}

	// default register callback
	if err := ksp.RegisterMessageCallback(&messageHandler{}); err != nil {
		log.Errorf("%v\n", errors.Wrap(err, "failed to register message callback."))
		os.Exit(0)
	}
	if err := ksp.SetCommHandler(&messageHandler{}); err != nil {
		log.Errorf("%v\n", errors.Wrap(err, "failed to register command callback."))
		os.Exit(0)
	}

	client.Name = name
	client.PK = ksp

	log.Outf("Wellcome %s!\n", client.Name)
	return nil
}

// Quick start.
func quickStart() error {
	didInfo, err := getActivedDidVerkey()
	if err != nil {
		return errors.Wrap(err, "failed to get actived did and verkey.")
	}

	if didInfo.Did != "" && didInfo.Verkey != "" {
		log.Warnf("Already Quick Start!\n")
		return nil
	}

	//inv := Input("Input a invitation:")
	//if inv == "" {
	//	inv = AlicePeerInv
	//}

	inv := AlicePeerInv
	connId, err := handleInvitation(inv)
	if err != nil {
		return errors.Wrap(err, "failed to handle invitation.")
	}

	conn, err := client.getConn(connId)
	if err != nil {
		return errors.Wrap(err, "failed to get connection.")
	}

	di, err := createPublicDid(false, "")
	if err != nil {
		return errors.Wrap(err, "failed to create public did.")
	}

	message := fmt.Sprintf(`{"type":"DidRegisterRequest","data":{"did":"%s","verkey":"%s"}}`, di.Did, di.Verkey)
	err = sendMessage(conn, message)
	if err != nil {
		return errors.New("failed to quick start register.")
	}
	return nil
}

///////////////////////////////////////// PRIVATE FUNCTIONS //////////////////////////////////////////////
// 1. get version
func getVersion() {
	if client.PK == nil {
		log.Warnf("Are you init your wallet?\n")
		log.Outf("SDK version is %s.\n", apint.API_VERSION)
		return
	}
	v := client.PK.GetVersion()
	log.Outf("SDK version is %s.\n", v)
}

// 2. show who am I.
func whoAmI(a string) {
	if client.PK == nil {
		log.Warnf("Are you init your wallet?\n")
		log.Outf("It's %s.\n", tmp.Name)
		return
	}

	if a == "" {
		log.Outf("It's %s.\n", client.Name)
		return
	}

	log.Outf("It's %s.\n", client.Name)

	di, err := getActivedDidVerkey()
	if err != nil {
		return
	}
	if di.Did != "" || di.Verkey != "" {
		log.Outf("Now use Did:%s Verkey:%s\n", client.DI.Did, client.DI.Verkey)
	}
}

// 2. List all messager.
func listAllMessager() {
	i := 0
	for k, v := range client.Conns {
		i++
		if v.Tip == "ROUTER" {
			log.Outf("%3d. name:%s(%s) conn id:%s\n", i, v.AliasName, v.Tip, k)
		} else {
			log.Outf("%3d. name:%s conn id:%s\n", i, v.AliasName, k)
		}
	}
	//client.PK.PrintConnections()
}

// 2. handle invitation
func handleInvitation(inv string) (string, error) {
	connId, err := client.PK.HandleInvitation(inv)
	if err != nil {
		return "", errors.Wrap(err, "failed to handle invitation.")
	}
	//conn, err := client.PK.GetConnection(connId)
	//if err != nil {
	//	return "", errors.Wrap(err, "failed to get connection.")
	//}

	//connAliasName := Input(fmt.Sprintf("please set a alias name for connection id(%s):", conn.ConnectionID))
	//if connAliasName == "" {
	//	connAliasName = "Unkown"
	//}
	//_conn := &_connection{
	//	connAliasName,
	//	"",
	//	conn,
	//}
	//TODO
	//client.Conns[connId] = _conn
	client.addConn(connId)

	return connId, nil
}

// 2. create invitation
func createInvitation(label, did string) (string, error) {
	if label != "" && did == "" {
		inv, err := client.PK.CreatePeerInvitation(label)
		if err != nil {
			log.Errorf("Create peer invitation by label(%s) failed!\n", label)
			return "", errors.Wrap(err, "failed to create peer invitation.")
		}
		time.Sleep(5 * time.Second)
		log.Infof("Create peer invitation by label(%s) success!\n", label)
		log.Outf("Peer invitation:%s\n", inv)
		return inv, nil
	}

	if label != "" && did != "" {
		inv, err := client.PK.CreateInvitation(label, did)
		if err != nil {
			log.Errorf("Create public invitation by label(%s) and did(%s) failed!\n", label, did)
			return "", errors.Wrap(err, "failed to create public invitation.")
		}
		log.Infof("Create pubic invitation by label(%s) and did(%s) success!\n", label, did)
		log.Outf("Public invitation:%s\n", inv)
		return inv, nil
	}

	log.Outf("create invitation invalid parameter(label=%s,did=%s).\n", label, did)
	return "", nil
}

// 3. active did verkey
func activeDidVerkey(did, verkey string) (*apint.DidInfo, error) {
	err := client.PK.UpdateActiveDid(did, verkey)
	if err != nil {
		return nil, err
	}

	di, err := getActivedDidVerkey()
	if err != nil {
		return nil, err
	}

	log.Outf("Active did success!\nNow use Did:%s Verkey:%s\n", did, verkey)
	return di, nil
}

// create a did
func createPublicDid(base bool, seed string) (*apint.DidInfo, error) {
	if base == false && seed != "" {
		di, err := client.PK.ImportPublicDid(seed)
		if err != nil {
			log.Errorf("Create did by seed(%s) failed!\n", seed)
			return nil, err
		}
		log.Infof("Create did by seed(%s) success!\n", seed)
		log.Outf("Did:%s Verkey:%s\n", di.Did, di.Verkey)
		return di, err
	}

	if base == true {
		di, err := client.PK.CreateAndRegisterPublicDid()
		if err != nil {
			log.Infof("Create and register did failed!\n")
			return nil, err
		}
		log.Infof("Create and register did success!\n")
		log.Outf("Did:%s Verkey:%s\n", di.Did, di.Verkey)
		return di, err
	}

	di, err := client.PK.CreatePublicDid()
	if err != nil {
		log.Infof("Create a did failed!\n")
		return nil, err
	}
	log.Infof("Create a did success!\n")
	log.Outf("Did:%s Verkey:%s\n", di.Did, di.Verkey)
	return di, err
}

// 4. send message
func sendMessage(conn *_connection, message string) error {
	if message != "close" {
		if err := client.PK.SendBasicMessage(conn.MyDID, conn.TheirDID, uuid.New().String(), message); err != nil {
			return errors.Wrap(err, "failed to send basic message")
		}
		return nil
	}
	return errors.New(CLOSEMESSAGE)
}

// 5. hold send message
func holdSendMessage(conn *_connection) error {
	log.Outf("Enter 'close' to close message.\n")
	for {
		message := Input(fmt.Sprintf("@%s:", conn.AliasName))
		if err := sendMessage(conn, message); err != nil {
			if err.Error() == CLOSEMESSAGE {
				return nil
			}
			return errors.Wrap(err, "failed to send basic message")
		}
	}
	return nil
}

// get actived did and verkey
func getActivedDidVerkey() (*apint.DidInfo, error) {
	useDid, err := client.PK.GetActiveDid()
	if err != nil {
		return nil, err
	}
	useVerkey, err := client.PK.GetActiveVerkey()
	if err != nil {
		return nil, err
	}
	di := &apint.DidInfo{useDid, useVerkey}
	client.DI = di

	return di, nil
}

// Register connection id with router
func registerWithRouter(name string) error {
	conn, err := client.getConn(name)
	if err != nil {
		return err
	}
	err = client.PK.RegisterWithRouter(conn.ConnectionID)
	if err != nil {
		return err
	}
	conn.Tip = "ROUTER"
	log.Outf("Register %s(%s) with router.\n", conn.AliasName, conn.ConnectionID)
	return nil
}

// Set connection alias name.
func setConnectionAliasName(connId string, name string) error {
	conn, err := client.getConn(connId)
	if err != nil {
		return err
	}
	conn.AliasName = name
	log.Outf("Set connection ID(%s) as alias name(%s).\n", conn.ConnectionID, name)
	return nil
}
