package apint

const (
	API_VERSION = "0.0.12"

	ROLE_TRUSTEE         = "0"   //indyvdri.TRUSTEE
	ROLE_STEWARD         = "2"   //indyvdri.STEWARD
	ROLE_ENDORSER        = "101" //indyvdri.ENDORSER
	ROLE_NETWORK_MONITOR = "201" //indyvdri.NETWORK_MONITOR
	ROLE_REMOVE          = ""    //indyvdri.ROLE_REMOVE
)

type (
	JsonObject struct {
		Id   string
		Json string
	}

	//DID 信息
	DidInfo struct {
		Did    string
		Verkey string
	}

	//连接信息
	Connection struct {
		ConnectionID string
		InvitationID string
		State        string
		TheirDID     string
		MyDID        string
	}

	//证书回调
	CredentialCallback interface {
		OnIssueCredential(proposalJson, from, to string) (string, error)
		OnVerifyPreview(previewJson, from, to string) (bool, error)
	}

	//证明回调
	ProofCallback interface {
		OnPresentCredentialAttrs(requestedCredentialsJson string) string
		OnPresentCredentialPreds(requestedCredentialsJson string) string
	}

	//通道连接回调
	CommCallback interface {
		OnConnection(connection *Connection)
	}

	//消息回调
	MessageCallback interface {
		OnBasicMessage(message, from, to string) error
	}

	//KCC SSI Plugin 接口
	KccSsiPlugin interface {
		//0. 新的 KCC SSI 实例
		NewKccSsiInstance()
		//1. 获取 API 版本
		GetVersion() string
		//2. 设置区块链本地名称
		SetPoolName(name string) error
		//3. 设置区块链 Genesis 文件
		SetGenesisFilePath(path string) error
		//4. 设置本地钱包名称
		SetWalletName(name string) error
		//5. 设置本地钱包路径
		SetWalletPath(path string) error
		//6. 设置 HTTP 通道信息
		SetHttpInbound(internalAddr, externalAddr string) error
		//7. 设置通道连接会回调 (连接建立通知)
		SetCommHandler(commHandler CommCallback) error
		//8. 设置证书会回调
		SetCredentialHandler(credentialHandler CredentialCallback) error
		//9. 设置证明回调
		SetProofHandler(proofHandler ProofCallback) error
		//10. 初始化 ZMQ 服务器
		InitZmqRouter(externalAddress, bindAddress string) error
		//11. 初始化 ZMQ 客户端
		InitZmqClient() error
		//12. 初始化 KCC SSI 实例
		Init() error
		//13. 获得当前激活的 DID
		GetActiveDid() (string, error)
		//14.  获得当前激活的 Verkey
		GetActiveVerkey() (string, error)
		//15. 更新激活的 DID
		UpdateActiveDid(did, verkey string) error
		//16. 设置需要激活的 DID 的角色
		SetActiveTargetRole(role string) error
		//17. 创建一个新的 DID
		CreatePublicDid() (*DidInfo, error)
		//18. 创建并注册一个带基本通信服务的 DID
		CreateAndRegisterPublicDid() (*DidInfo, error)
		//19. 导入公共 DID
		ImportPublicDid(seed string) (*DidInfo, error)
		//20. 创建点对点通信邀请 (返回邀请字符串)
		CreatePeerInvitation(label string) (string, error)
		//21. 创建 通信邀请 (返回邀请字符串)
		CreateInvitation(label string, did string) (string, error)
		//22. 处理邀请信息 (返回连接ID)
		HandleInvitation(invitationStr string) (string, error)
		//23. 根据 CONNECT ID 获取连接
		GetConnection(id string) (*Connection, error)
		//24. 打印所有连接
		PrintConnections() error
		//25. 根据连接获取索引
		GetConnectionByIndex(idx int) (*Connection, error)
		//26. 释放连接集合
		ReleaseConnections() error
		//27. 获取所有连接
		GetConnections() (int, error)
		//28. 根据 Label 获取连接
		GetConnectionByLabel(label string) (*Connection, error)
		//29. 根据 DID 获取 服务接入信息
		GetServiceEndpoint(did string) error
		//30.
		CheckRouter() error
		//31. 将连接的对方注册为自己的 ROUTER
		RegisterWithRouter(connectionId string) error
		//32. 取消注册的 ROUTER
		UnregisterWithRouter() error
		//33. 注册 DID
		RegisterDid(did, verkey, role string) error
		//34. 创建证书格式
		CreateSchema(issuerDid, schemaName, schemaVersion string, schemaAttributes []string) (string, error)
		//35. 创建 Topic
		CreateTopic(connectionId string) (string, error)
		//36. 订阅 Topic
		SubscribeTopic(connectionId string, did string) error
		//37. 取消订阅  Topic
		UnsubscribeTopic(connectionId string, did string) error
		//38. 获取证书格式
		FetchSchema(issuerDid, schemaName, schemaVersion string) (*JsonObject, error)
		//39. 创建证书定义
		CreateCredDef(schemaJson, tag, signatureType string) (*JsonObject, error)
		//40. 注册消息回调
		RegisterMessageCallback(callback MessageCallback) error
		//41. 发送基本消息
		SendBasicMessage(myDid, theirDid, messageId, message string) error
		//42. 发送证书预览
		SendCredentialProposal(conn *Connection, credDefId, comment string) error
		//43. 测试回调
		TestCallback(message string) error
	}
)
