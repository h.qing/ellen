package main

import (
	"context"

	"kuaicuocuo.com/ellen/client"
)

func main() {
	ctx := context.Background()
	client.Start(ctx)
}
